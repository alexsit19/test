package com.example.testkmm

interface Platform {
    val name: String
}

expect fun getPlatform(): Platform